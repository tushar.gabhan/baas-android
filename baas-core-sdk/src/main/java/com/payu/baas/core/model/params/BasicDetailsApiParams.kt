package com.payu.baas.core.model.params

import com.payu.baas.core.model.ApiParams

class BasicDetailsApiParams: ApiParams() {
    var brandTokenId: String? = null
    var brandEmpId: String? = null
}